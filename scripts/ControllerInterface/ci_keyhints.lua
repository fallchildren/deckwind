local I = require("openmw.interfaces")

local const = require("scripts.ControllerInterface.ci_const")
if not const.doesRun then
    return {}
end

local function getActorServices(actor)
    local record = actor.type.records[actor.recordId]
    if not record then
        return {}
    end
    local services = record.servicesOffered
    local validServices = {}
    if services["Training"] then
        table.insert(validServices, "Training")
    end
    if services["Travel"] then
        table.insert(validServices, "Travel")
    end
    if services["Enchanting"] then
        table.insert(validServices, "Enchanting")
    end
    if services["Spells"] then
        table.insert(validServices, "Spells")
    end
    local barter = false
    if  services["MagicItems"] then
        barter = true
    elseif  services["RepairItems"] then
        barter = true
    elseif  services["Weapon"] then
        barter = true
    elseif  services["Armor"] then
        barter = true
    elseif services["Ingredients"] then
        barter = true
    elseif  services["Books"] then
        barter = true
    elseif  services["Picks"] then
        barter = true
    elseif  services["Probes"] then
        barter = true
    elseif services["Apparatus"] then
        barter = true
    elseif services["Misc"] then
        barter = true
    elseif services["Potions"] then
        barter = true
    elseif services["Lights"] then
        barter = true
    elseif services["Clothing"] then
        barter = true
    end
    if barter then
        table.insert(validServices, "Barter")
    end
    --This needs to respect service refsual
    return validServices
end
local uiHints = {
    Journal = {
        DPadUp = "Previous Page",
        DPadDown = "Next Page",
        Y = "Open Quest Menu",
    },
    SpellBuying = {

        DPadUp = "Navigate List Up",
        DPadDown = "Navigate List Down",
        A = "Purchase Spell",
        B = "Exit Spell Buying",
    },
    Dialogue = {

    }
}

local function UiModeChanged(data)
    
    local newMode = data.newMode
    I.CI_Background.clearHotKeys()
    
    if newMode and uiHints[newMode] then
        for index, value in pairs(uiHints[newMode]) do
            I.CI_Background.addHotKey(value,index)
        end
        if newMode == "Dialogue" then
            local services = getActorServices(data.arg)
            if services then
                if services[1] then
                    I.CI_Background.addHotKey(services[1], "Y")
                end
                if services[2] then
                    I.CI_Background.addHotKey(services[2], "X")
                end
            end
        end
        I.CI_Background.drawBottomBar()
        print('read book')
    end
end

return {
    eventHandlers = {
        UiModeChanged = UiModeChanged
    }
}
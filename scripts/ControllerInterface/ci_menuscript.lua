local menu = require('openmw.menu')
local input = require("openmw.input")
local function loadMostRecent()
    local mostRecentSave = nil
    local mostRecentTime = 0
    for folder, saveList in pairs(menu.getAllSaves()) do
        for name, save in pairs(saveList) do
            if not mostRecentSave or save.creationTime > mostRecentTime then
                mostRecentSave = { folder = folder, name = name, creationTime = save.creationTime }
                mostRecentTime = save.creationTime
            end
        end
    end

    if mostRecentSave then
      --  print(mostRecentSave.creationTime, "most recent", mostRecentSave.folder, mostRecentSave.name)
        menu.loadGame(mostRecentSave.folder, mostRecentSave.name)
    else
        print("No saves found.")
    end
end

local function doQuickSave()
    menu.saveGame("Quicksave","quicksave")
end
local function onStateChanged()
    local state = menu.getState()

    if state == menu.STATE.NoGame then
        input._setGamepadCursorActive(false)
    end
end
return{
    eventHandlers = {
        doQuickSave = doQuickSave,
    },
    engineHandlers = {
        onStateChanged = onStateChanged,
        onControllerButtonPress = function (ctrl)
            if ctrl == input.CONTROLLER_BUTTON.Y and menu.getState() == menu.STATE.NoGame then
                loadMostRecent()
            end
            if  input._isGamepadCursorActive() and (ctrl == input.CONTROLLER_BUTTON.LeftShoulder or ctrl == input.CONTROLLER_BUTTON.RightShoulder) then
              
        input._setGamepadCursorActive(false)
            end
            
        end
    }
}
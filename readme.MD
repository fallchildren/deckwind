# Deckwind Controller Mod

## Features

- Replaces the Inventory, container, barter menus, with a controller-focused menu.
- Replaces the spell menu with a new controller menu
- Adds the ability to press "Y" from the main menu to load your most recent save
- 
- ...
## List of Windows and their Controller friendliness
- Trade: Replaced, fully Controller Support
- Container: Replaced, fully Controller Support
- WaitDialog: Not replaced, but is already navigble with the DPad
- SpellBuying: Fully replaced, full controller support
- Training: Not replaced, but is already navigable with shoulder buttons/DPad
- MerchantRepair: Fully replaced, full controller support
- Magic: Replaced, fully Controller Support
- Recharge: Not replaced, but very feasible to do so
- Map: Not replacable. 
- Book: Not replaced, but is already navigable with shoulder buttons/DPad
- Travel: Not replaced, but is already navigable with shoulder buttons/DPad
- Inventory: Replaced, fully Controller Support
- Journal: Not replaced. Poor controller support. May be doable with new MR coming soon.
- Dialogue: Not replaced. Controller support only for selecting topics. Requires that you have hightlighting for already used and new topics turned off for the highlighting for selection to work correctly.
- Alchemy: Not replaced. Poor controller support. 
- Scroll: Not replaced, but is already navigable with shoulder buttons/DPad
- Companion: Replaced, fully Controller Support
- EnchantingDialog: Not replaced. Not controller friendly. Could be done down the line, once new enchant records could be created.
- JailScreen: No actual input here.
- LevelUpDialog: Not replaced, but is already navigable with shoulder buttons/DPad
- Repair: Not replaced, but very feasible to do so
- QuickKeys: Replaced, you can favorite items from your inventory/spells list(using Quickselect)
- SpellCreationDialog: Not replaced. Not controller friendly. Could be done down the line, once new spell records could be created.
- Stats: Not replaced, but very doable. Not too bad for controllers anyway.
## Installation

1. Download the latest release of the Deckwind mod from [GitHub](https://github.com/your-username/Deckwind/releases).
2. Extract the downloaded ZIP file.
3. Copy the extracted files to the `Data Files` directory of your OpenMW installation.
4. Launch OpenMW and enable the Deckwind mod in the mod manager.

## Usage

1. Connect your controller to your computer.
2. Launch OpenMW and navigate to the settings menu.
3. Select the "Controls" tab.
4. Configure the Deckwind mod settings according to your preferences.
5. Save the settings and start playing the game with your controller.

## Troubleshooting

If you encounter any issues or have questions, please refer to the [FAQ](https://github.com/your-username/Deckwind/wiki/FAQ) section on the GitHub repository page.

## Contributing

Contributions are welcome! If you would like to contribute to the Deckwind mod, please follow the guidelines outlined in the [CONTRIBUTING.md](https://github.com/your-username/Deckwind/blob/main/CONTRIBUTING.md) file.

## License

This mod is licensed under the [MIT License](https://github.com/your-username/Deckwind/blob/main/LICENSE).